from flask import Flask, jsonify
from httpx_app import get_all_berry_stats
import json

app = Flask(__name__)

@app.get('/allBerryStats')
def all_berry_stats():
    try:
        berry_stats = get_all_berry_stats()
        return jsonify(berry_stats) # jsonify includes the application/json header

    except Exception as e:
        return jsonify({"Error": str(e)}), 500


if __name__ == '__main__':
    app.run(debug=True)

# PokeAPI client
This is an excercise to implement a simple Flask REST API client to get
statistic data from pokeapi.co

## Installation
Just clone this repo, create a virtual environment, activate it an install the
requirements

```
$ git clone 
$ python3 -m venv venv
$ . venv/bin/activate
$ pip install -r requirements.txt
```

## Execution

You can start the local server by typing

```
$ python flask-app.py
```

then you can open a web browser and make the get request through the url

```
http://127.0.0.1:5000/allBerryStats
```

you should see the list of berry names and a number of statistics about growth times

## Run test
A simple test is included. You can execute it like this

```
$ pytest test_app.py
```


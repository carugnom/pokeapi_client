import pytest
import os
import json
from unittest.mock import patch
from httpx_app import get_all_berry_stats, BerryException
from httpx import Response


def test_get_all_berry_stats():
    # mock the lower-level httpx.get() only while calling our endpoint method
    mock_responses = {
        'https://pokeapi.co/api/v2/berry': {'count': 4},
        'https://pokeapi.co/api/v2/berry/1': {'name': 'berry1', 'growth_time': 10},
        'https://pokeapi.co/api/v2/berry/2': {'name': 'berry2', 'growth_time': 15},
        'https://pokeapi.co/api/v2/berry/3': {'name': 'berry3', 'growth_time': 15},
        'https://pokeapi.co/api/v2/berry/4': {'name': 'berry4', 'growth_time': 99},
    }

    def mocked_get(url):
        response_data = mock_responses[url]
        return Response(content=json.dumps(response_data), status_code=200)

    with patch('httpx_app.httpx.AsyncClient.get') as mock_get:
        mock_get.side_effect = mocked_get
        berry_stats = get_all_berry_stats()   # method to test

    assert len(berry_stats["berries_names"]) == 4
    assert berry_stats["min_growth_time"] == 10
    assert berry_stats["median_growth_time"] == 15
    assert berry_stats["max_growth_time"] == 99
    assert berry_stats["variance_growth_time"] == 1380.1875
    assert berry_stats["mean_growth_time"] == 34.75
    assert berry_stats["frequency_growth_time"][10] == 1
    assert berry_stats["frequency_growth_time"][15] == 2
    assert berry_stats["frequency_growth_time"][99] == 1


def test_404():
    def mocked_get(url):
        return Response(status_code=404)

    # patch the lower-level httpx.get() only while calling our endpoint method
    with patch('httpx_app.httpx.AsyncClient.get') as mock_get:
        mock_get.side_effect = mocked_get

        with pytest.raises(BerryException):
            berry_stats = get_all_berry_stats()   # method to test


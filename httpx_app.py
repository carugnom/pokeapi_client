import httpx
import os
import asyncio
import json
import numpy as np

class BerryException(Exception):
    pass


async def get_berry_count():
    api_url = os.getenv("POKE_API_URL", "https://pokeapi.co/api/v2/berry")

    try:
        async with httpx.AsyncClient() as client:
            resp = await client.get(f"{api_url}")
            if resp.status_code != 200:
                raise BerryException(f"HTTPStatusError: {resp.status_code}")
            resp_dict = resp.json()
            berry_count = resp_dict["count"]
            return berry_count

    except httpx.HTTPStatusError as e:
        raise BerryException(f"HTTPStatusError: {e.response.status_code}, {e.response.text}") from e

    except httpx.RequestError as e:
        raise BerryException(f"RequestError: {str(e)}") from e

    except Exception as e:
        raise e


async def get_berry(_id):
    api_url = os.getenv("POKE_API_URL", "https://pokeapi.co/api/v2/berry")

    try:
        async with httpx.AsyncClient() as client:
            resp = await client.get(f"{api_url}/{_id}")
            if resp.status_code != 200:
                raise BerryException(f"HTTPStatusError: {resp.status_code}")
            resp_dict = resp.json()
            name = resp_dict['name'] 
            growth_time = resp_dict['growth_time'] 
            return (name, growth_time)

    except httpx.HTTPStatusError as e:
        raise BerryException(f"HTTPStatusError: {e.response.status_code}, {e.response.text}") from e

    except httpx.RequestError as e:
        raise BerryException(f"RequestError: {str(e)}") from e

    except Exception as e:
        raise e


async def get_all_berries():
    berry_count = await get_berry_count()

    async with httpx.AsyncClient() as client:
        tasks = [get_berry(_id) for _id in range(1, berry_count+1)]
        berries = await asyncio.gather(*tasks)

    return berries


async def get_berry_stats():
    growth_times_by_berry = await get_all_berries()
    berries_names = [berry_growth_time[0] for berry_growth_time in growth_times_by_berry]
    growth_times = [berry_growth_time[1] for berry_growth_time in growth_times_by_berry]

    # calculations
    min_growth_time = np.min(growth_times)
    max_growth_time = np.max(growth_times)
    median_growth_time = np.median(growth_times)
    mean_growth_time = np.mean(growth_times)
    variance_growth_time = np.var(growth_times)
    unique_growth_times, frequencies = np.unique(growth_times, return_counts=True)
    frequency_growth_time = dict(zip(unique_growth_times.tolist(), frequencies.tolist()))

    # build response
    response = {
        "berries_names": berries_names,
        "min_growth_time": int(min_growth_time),
        "median_growth_time": float(median_growth_time),
        "max_growth_time": int(max_growth_time),
        "variance_growth_time": float(variance_growth_time),
        "mean_growth_time": float(mean_growth_time),
        "frequency_growth_time": frequency_growth_time
    }

    return response
    

def get_all_berry_stats():
    return asyncio.run(get_berry_stats())


if __name__ == "__main__":
    print(get_all_berry_stats())

